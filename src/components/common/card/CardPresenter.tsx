import React, {FC} from 'react'
import Card from "../../../interface/Card.interface";

interface Props {
  card: Card
}

const CardPresenter: FC<Props> = ({card}) => {
  return (<div>
    <img src={'/img/bang/cards/' + card.id + '.png'} alt={card.id}/>
  </div>)
}

export default CardPresenter
