import React, {FC} from 'react'
import ControllingPlayer from "../../interface/ControllingPlayer.interface";
import HandControl from "./hand-control/HandControl";
import TableControl from "./table-control/TableControl";
import CharacterControl from "./character-control/CharacterControl";

interface Props {
  player: ControllingPlayer
}

const PlayerControl: FC<Props> = ({player}) => {
  return (<div>
    <HandControl cards={player.handCards}/>
    <TableControl cards={player.tableCards}/>
    <CharacterControl character={player.character}/>
  </div>)
}

export default PlayerControl
