import React, {FC} from 'react'
import Card from "../../../interface/Card.interface";
import CardPresenter from "../../common/card/CardPresenter";

interface Props {
  cards: Card[]
}

const TableControl: FC<Props> = ({cards}) => {
  return (<div>
    {cards.map(card => <CardPresenter card={card}/>)}
  </div>)
}

export default TableControl
