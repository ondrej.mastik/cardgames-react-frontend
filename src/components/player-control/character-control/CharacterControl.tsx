import React, {FC} from 'react'
import {Character} from "../../../enum/character.enum";

interface Props {
  character: Character
}

const CharacterControl: FC<Props> = ({character}) => {
  return (<div>
    <img src={'/img/bang/characters/' + character + '.png'} alt={character}/>
  </div>)
}

export default CharacterControl
