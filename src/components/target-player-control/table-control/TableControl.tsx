import React, {FC} from 'react'
import Card from "../../../interface/Card.interface";

interface Props {
  cards: Card[]
}

const TableControl: FC<Props> = ({cards}) => {
  return (<div>
    {cards.map(value => value.id).join(", ")}
  </div>)
}

export default TableControl
