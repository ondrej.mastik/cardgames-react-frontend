import React, {FC} from 'react'

interface Props {
  cardsCount: number
}

const HandControl: FC<Props> = ({cardsCount}) => {
  return (<div>
    cards count: {cardsCount}
  </div>)
}

export default HandControl
