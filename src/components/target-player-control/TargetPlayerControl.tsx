import React, {FC} from 'react'
import TargetPlayer from "../../interface/TargetPlayer.interface";
import HandControl from "./hand-control/HandControl";
import TableControl from "./table-control/TableControl";
import RoleControl from "./role-control/RoleControl";
import CharacterControl from "./character-control/CharacterControl";

interface Props {
  player: TargetPlayer
}

const TargetPlayerControl: FC<Props> = ({player}) => {
  return (<div>
    <HandControl cardsCount={player.handCardsCount}/>
    <TableControl cards={player.tableCards}/>
    <RoleControl role={player.role}/>
    <CharacterControl character={player.character} hp={player.hp}/>
  </div>)
}

export default TargetPlayerControl
