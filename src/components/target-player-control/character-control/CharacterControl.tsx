import React, {FC} from 'react'
import {Character} from "../../../enum/character.enum";

interface Props {
  character: Character
  hp: number
}

const CharacterControl: FC<Props> = ({character, hp}) => {
  return (<div>
    {character} has {hp} hp
  </div>)
}

export default CharacterControl
