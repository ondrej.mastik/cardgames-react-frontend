import React, {FC} from 'react'
import {Role} from "../../../enum/role.enum";

interface Props {
  role?: Role
}

const RoleControl: FC<Props> = ({role}) => {
  return (<div>
    {role}
  </div>)
}

export default RoleControl
