import React, {FC, useState} from 'react'
import Game from "../interface/Game.interface";
import PlayerControl from "./player-control/PlayerControl";
import TargetPlayerControl from "./target-player-control/TargetPlayerControl";
import CardPresenter from "./common/card/CardPresenter";
import {Character} from "../enum/character.enum";
import {Role} from "../enum/role.enum";

const GameContainer: FC = () => {
  const [game] = useState<Game>({
    player: {
      hp: 4,
      character: Character.WillyTheKid,
      id: 1,
      nickname: "Borec123",
      role: Role.Sheriff,
      tableCards: [{
        id: 'appaloosa',
        number: '3',
        type: 1
      },{
        id: 'winchester',
        number: '3',
        type: 1
      }],
      handCards: [{
        id: 'birra',
        number: '3',
        type: 1
      },{
        id: 'diligenza',
        number: '3',
        type: 1
      }]
    },
    targetPlayers: [
      {
        hp: 3,
        character: Character.BartCassidy,
        id: 2,
        nickname: "Lama123",
        role: Role.Sheriff,
        tableCards: [{
          id: 'remington',
          number: '3',
          type: 1
        },{
          id: 'dinamite',
          number: '3',
          type: 1
        }],
        handCardsCount: 3
      }
    ]
  } as Game)

  return (<div>
    {game.lastDiscardedCard && <CardPresenter card={game.lastDiscardedCard}/>}
    <PlayerControl player={game.player}/>
    {game.targetPlayers.map(player => <TargetPlayerControl player={player}/>)}
  </div>)
}

export default GameContainer
