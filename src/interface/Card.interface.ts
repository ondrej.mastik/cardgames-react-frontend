export default interface Card {
  id: string
  type: number
  number: string
}