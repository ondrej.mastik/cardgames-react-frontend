import Player from "./Player.interface";
import {Role} from "../enum/role.enum";

export default interface TargetPlayer extends Player {
  role?: Role
  handCardsCount: number
}