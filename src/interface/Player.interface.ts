import Card from "./Card.interface";
import {Character} from "../enum/character.enum";

export default interface Player {
  id: number
  nickname: string
  tableCards: Card[]
  character: Character
  hp: number
}