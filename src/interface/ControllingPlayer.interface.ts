import Player from "./Player.interface";
import Card from "./Card.interface";
import {Role} from "../enum/role.enum";

export default interface ControllingPlayer extends Player {
  handCards: Card[]
  role: Role
}