import Card from "./Card.interface";
import ControllingPlayer from "./ControllingPlayer.interface";
import TargetPlayer from "./TargetPlayer.interface";

export default interface Game {
  lastDiscardedCard?: Card
  player: ControllingPlayer
  targetPlayers: TargetPlayer[]
}